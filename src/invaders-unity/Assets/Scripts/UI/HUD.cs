using System;
using System.Collections.Generic;
using Core.Application.Factories;
using UnityEngine;
using Zenject;

namespace UI
{
    /*
     * This is a domain of all UI in game.
     * There are couples of screens and a logic to change them.
     */

    interface IHUD
    {
        void Open(Type screen);
    }

    sealed class HUD : MonoBehaviour, IHUD
    {
        [SerializeField] Canvas Canvas;
        [SerializeField] GameObject StartScreen;
        [SerializeField] GameObject InGameScreen;
        [SerializeField] GameObject VictoryScreen;
        [SerializeField] GameObject GameOverScreen;

        [Inject] readonly IPrefabFactory _factory;
        
        GameObject _currentScreen;
        Dictionary<Type, GameObject> _screenPrefabs;

        void Awake()
        {
            _screenPrefabs = new Dictionary<Type, GameObject>
            {
                { Screens.StartScreen, StartScreen },
                { Screens.InGameScreen, InGameScreen },
                { Screens.VictoryScreen, VictoryScreen },
                { Screens.GameOverScreen, GameOverScreen }
            };
        }

        void IHUD.Open(Type screen)
        {
            if (_currentScreen)
                Destroy(_currentScreen);

            if (_screenPrefabs.TryGetValue(screen, out var prefab))
                _currentScreen = _factory.Create(prefab, Canvas.transform);
            else
                throw new InvalidOperationException($"There is no registered screen for type {screen}");
        }
    }
}