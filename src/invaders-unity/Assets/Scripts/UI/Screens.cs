using System;
using UI.ScreenLogic.GameOver;
using UI.ScreenLogic.InGame;
using UI.ScreenLogic.Start;
using UI.ScreenLogic.Victory;

namespace UI
{
    static class Screens
    {
        public static readonly Type StartScreen = typeof(IStartScreen);
        public static readonly Type InGameScreen = typeof(IInGameScreen);
        public static readonly Type VictoryScreen = typeof(IVictoryScreen);
        public static readonly Type GameOverScreen = typeof(IGameOverScreen);
    }
}