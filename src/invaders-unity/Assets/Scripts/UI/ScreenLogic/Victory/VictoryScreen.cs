using System;
using System.Collections;
using Core.Mvp;
using TMPro;
using UnityEngine;
using Zenject;

namespace UI.ScreenLogic.Victory
{
    interface IVictoryScreen : IView
    {
        event Action OnRetryPressed;
        int Score { set; }
    }

    sealed class VictoryScreen : View, IVictoryScreen
    {
        [SerializeField] TextMeshProUGUI ScoreLabel;

        [Inject] VictoryScreenPresenter.Factory _factory;
        protected override IPresenter NewPresenter => _factory.Create(this);

        public event Action OnRetryPressed;

        int IVictoryScreen.Score { set => ScoreLabel.text = value.ToString(); }

        public void OnRetryButtonPressed() => StartCoroutine(PressCoroutine());

        IEnumerator PressCoroutine()
        {
            yield return new WaitForSeconds(0.3f);
            OnRetryPressed?.Invoke();
        }
    }
}