using Core.Mvp;
using Zenject;

namespace UI.ScreenLogic.Victory
{
    sealed class VictoryScreenPresenter : ScreenPresenter<IVictoryScreen>
    {
        public VictoryScreenPresenter(IVictoryScreen view) : base(view) { }

        protected override void Subscribe() => View.OnRetryPressed += HandleOnRetryPressed;

        protected override void Unsubscribe() => View.OnRetryPressed -= HandleOnRetryPressed;

        public override void Start() => View.Score = Model.Score.Value;

        void HandleOnRetryPressed()
        {
            Model.Level.Unload();
            HUD.Open(Screens.StartScreen);
        }

        public sealed class Factory : PlaceholderFactory<IVictoryScreen, VictoryScreenPresenter> { }
    }
}