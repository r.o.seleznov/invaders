using System;
using System.Collections;
using Core.Mvp;
using UnityEngine;
using Zenject;

namespace UI.ScreenLogic.GameOver
{
    interface IGameOverScreen : IView
    {
        event Action OnRetryPressed;
    }

    sealed class GameOverScreen : View, IGameOverScreen
    {
        [Inject] GameOverScreenPresenter.Factory _factory;
        protected override IPresenter NewPresenter => _factory.Create(this);
        
        public event Action OnRetryPressed;

        public void OnRetryButtonPressed() => StartCoroutine(PressCoroutine());

        IEnumerator PressCoroutine()
        {
            yield return new WaitForSeconds(0.3f);
            OnRetryPressed?.Invoke();
        }
    }
}