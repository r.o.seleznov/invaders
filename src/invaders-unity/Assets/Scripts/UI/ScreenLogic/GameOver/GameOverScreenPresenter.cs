using Core.Mvp;
using Zenject;

namespace UI.ScreenLogic.GameOver
{
    sealed class GameOverScreenPresenter : ScreenPresenter<IGameOverScreen>
    {
        public GameOverScreenPresenter(IGameOverScreen view) : base(view) { }

        protected override void Subscribe() => View.OnRetryPressed += HandleOnRetryPressed;

        protected override void Unsubscribe() => View.OnRetryPressed -= HandleOnRetryPressed;

        void HandleOnRetryPressed()
        {
            Model.Level.Unload();
            HUD.Open(Screens.StartScreen);
        }

        public sealed class Factory : PlaceholderFactory<IGameOverScreen, GameOverScreenPresenter> { }
    }
}