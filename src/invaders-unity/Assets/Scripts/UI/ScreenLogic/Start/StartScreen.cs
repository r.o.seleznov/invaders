using System;
using System.Collections;
using Core.Mvp;
using Mechanics.EnemyLogic;
using Mechanics.UfoLogic;
using TMPro;
using UnityEngine;
using Zenject;

namespace UI.ScreenLogic.Start
{
    interface IStartScreen : IView
    {
        event Action OnStartPressed;
    }

    sealed class StartScreen : View, IStartScreen
    {
        [Inject] StartScreenPresenter.Factory _factory;
        protected override IPresenter NewPresenter => _factory.Create(this);
        
        [SerializeField] UfoType UfoType;
        [SerializeField] EnemyType CrabType;
        [SerializeField] EnemyType SquidType;
        [SerializeField] EnemyType OctopusType;

        [SerializeField] TextMeshProUGUI UfoScore;
        [SerializeField] TextMeshProUGUI CrabScore;
        [SerializeField] TextMeshProUGUI SquidScore;
        [SerializeField] TextMeshProUGUI OctopusScore;

        public event Action OnStartPressed;

        protected override void OnAwake()
        {
            UfoScore.text = UfoType.Data.ScorePoints.ToString();
            CrabScore.text = CrabType.ScorePoints.ToString();
            SquidScore.text = SquidType.ScorePoints.ToString();
            OctopusScore.text = OctopusType.ScorePoints.ToString();
        }

        public void OnStartButtonPressed() => StartCoroutine(PressCoroutine());

        IEnumerator PressCoroutine()
        {
            yield return new WaitForSeconds(0.3f);
            OnStartPressed?.Invoke();
        }
    }
}