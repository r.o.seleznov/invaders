using Core.Application.LifeCycle;
using Core.Mvp;
using Zenject;

namespace UI.ScreenLogic.Start
{
    sealed class StartScreenPresenter : ScreenPresenter<IStartScreen>
    {
        [Inject] IAppLifeCycle _lifeCycle;

        public StartScreenPresenter(IStartScreen screen) : base(screen) { }

        protected override void Subscribe() => View.OnStartPressed += HandleOnStartPressed;

        protected override void Unsubscribe() => View.OnStartPressed -= HandleOnStartPressed;

        void HandleOnStartPressed()
        {
            _lifeCycle.Start();
            HUD.Open(Screens.InGameScreen);
        }

        public sealed class Factory : PlaceholderFactory<IStartScreen, StartScreenPresenter> { }
    }
}