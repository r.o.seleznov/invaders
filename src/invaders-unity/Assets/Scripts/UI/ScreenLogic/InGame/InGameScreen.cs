﻿using Core.Mvp;
using TMPro;
using UnityEngine;
using Zenject;

namespace UI.ScreenLogic.InGame
{
    interface IInGameScreen : IView
    {
        int Score { set; }
    }

    sealed class InGameScreen : View, IInGameScreen
    {
        [SerializeField] TextMeshProUGUI ScoreLabel;

        [Inject] InGameScreenPresenter.Factory _factory;
        protected override IPresenter NewPresenter => _factory.Create(this);

        int IInGameScreen.Score { set => ScoreLabel.text = value.ToString(); }
    }
}