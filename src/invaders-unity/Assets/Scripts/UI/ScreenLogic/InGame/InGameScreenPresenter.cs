using System;
using Core.Mvp;
using Zenject;

namespace UI.ScreenLogic.InGame
{
    sealed class InGameScreenPresenter : ScreenPresenter<IInGameScreen>
    {
        public InGameScreenPresenter(IInGameScreen view) : base(view) { }

        protected override void Subscribe()
        {
            Model.Score.OnChanged += HandleScoreOnChanged;
            Model.Level.OnVictory += HandleLevelOnVictory;
            Model.Level.OnGameOver += HandleLevelOnGameOver;
        }

        protected override void Unsubscribe()
        {
            Model.Score.OnChanged -= HandleScoreOnChanged;
            Model.Level.OnVictory -= HandleLevelOnVictory;
            Model.Level.OnGameOver -= HandleLevelOnGameOver;
        }

        void HandleScoreOnChanged(int value) => View.Score = value;

        void HandleLevelOnVictory() => Goto(Screens.VictoryScreen);

        void HandleLevelOnGameOver() => Goto(Screens.GameOverScreen);

        void Goto(Type screen)
        {
            Model.Level.Unload();
            HUD.Open(screen);
        }

        public sealed class Factory : PlaceholderFactory<IInGameScreen, InGameScreenPresenter> { }
    }
}