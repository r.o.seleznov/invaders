using System.Collections.Generic;

namespace Utils
{
    static class QueueExtensions
    {
        public static bool TryDequeue<T>(this Queue<T> queue, out T result)
        {
            result = default;

            bool hasAny = queue.Count > 0;
            if (hasAny)
                result = queue.Dequeue();

            return hasAny;
        }
    }
}