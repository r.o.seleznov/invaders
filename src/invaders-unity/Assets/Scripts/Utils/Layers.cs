using UnityEngine;

namespace Utils
{
    struct Layer
    {
        public static Layer Wall = "Wall";
        public static Layer Enemy = "Enemy";
        public static Layer WallUfo = "WallUfo";
        public static Layer WallOrbit = "WallOrbit";
        public static Layer EnemyProjectile = "EnemyProjectile";
        public static Layer PlayerProjectile = "PlayerProjectile";

        readonly int _index;

        Layer(string name) => _index = LayerMask.NameToLayer(name);

        public static implicit operator int(Layer id) => id._index;

        public static implicit operator Layer(string name) => new Layer(name);
    }
}