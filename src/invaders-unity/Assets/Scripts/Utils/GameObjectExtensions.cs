using UnityEngine;

namespace Utils
{
    static class GameObjectExtensions
    {
        public static T GetComponentInChildrenOrAdd<T>(this GameObject go) where T : Component
        {
            var component = go.GetComponentInChildren<T>();
            if (component == null)
                component = go.AddComponent<T>();

            return component;
        }
    }
}