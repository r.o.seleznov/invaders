using System.Collections;
using UnityEngine;

namespace Utils
{
    /*
     * This abstraction help us to run coroutines from anywhere in the code.
     * It delegates them to stored behaviour. In this game it is a AppInstaller which is always alive for simplicity.
     */

    interface ICoroutines
    {
        Coroutine Start(IEnumerator coroutine);
        void Stop(ref Coroutine coroutine);
    }

    sealed class Coroutines : ICoroutines
    {
        readonly MonoBehaviour _behaviour;

        public Coroutines(MonoBehaviour behaviour) => _behaviour = behaviour;

        Coroutine ICoroutines.Start(IEnumerator coroutine) => _behaviour.StartCoroutine(coroutine);

        void ICoroutines.Stop(ref Coroutine coroutine)
        {
            if (coroutine != null)
                _behaviour.StopCoroutine(coroutine);

            coroutine = null;
        }
    }
}