using System.Linq;

namespace Utils
{
    static class Reflection
    {
        public static TProperty[] PropertiesOfType<TProperty>(this object instance) =>
            instance.GetType().GetProperties()
                .Where(x => typeof(TProperty).IsAssignableFrom(x.PropertyType))
                .Select(x => (TProperty)x.GetValue(instance))
                .ToArray();
    }
}