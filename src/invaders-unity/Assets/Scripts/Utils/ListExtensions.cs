using System;
using System.Collections.Generic;

namespace Utils
{
    static class ListExtensions
    {
        public static T RemoveFirst<T>(this List<T> list, Predicate<T> match)
        {
            int index = list.FindIndex(match);
            if (index < 0)
                return default;

            var removedItem = list[index];
            list.RemoveAt(index);

            return removedItem;
        }
    }
}