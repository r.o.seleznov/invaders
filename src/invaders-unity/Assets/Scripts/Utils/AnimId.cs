using UnityEngine;

namespace Utils
{
    /*
     * A simple abstraction to store an id of Animator param.
     */
    struct AnimId
    {
        readonly int _hash;

        AnimId(string name) => _hash = Animator.StringToHash(name);

        public static implicit operator int(AnimId id) => id._hash;

        public static implicit operator AnimId(string name) => new AnimId(name);
    }
}