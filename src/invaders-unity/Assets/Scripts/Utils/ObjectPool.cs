using System;
using System.Collections.Generic;
using Object = UnityEngine.Object;

namespace Utils
{
    class ObjectPool<T> where T : Object
    {
        public event Action<T> OnReturn;
        public event Action<T> OnGet;
        
        readonly Func<T> _createNew;
        readonly Queue<T> _queue = new();

        protected ObjectPool(Func<T> createNew) => _createNew = createNew;

        public bool Return(T obj)
        {
            bool canReturn = !_queue.Contains(obj);
            if (canReturn)
            {
                _queue.Enqueue(obj);
                OnReturn?.Invoke(obj);
            }

            return canReturn;
        }

        public T Get()
        {
            var result = _queue.TryDequeue(out var fromQueue) ? fromQueue : NewObject();
            OnGet?.Invoke(result);
            return result;
        }

        T NewObject() => _createNew?.Invoke();
    }
}