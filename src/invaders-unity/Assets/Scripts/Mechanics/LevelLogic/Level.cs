﻿using System;
using Core.Application.LifeCycle;
using Core.Application.Loaders;
using Mechanics.ArmyLogic;
using Mechanics.PlayerLogic;
using Zenject;

namespace Mechanics.LevelLogic
{
    /*
     * Level is a mechanism to deal with Victory and GameOver states.
     * It subscribed to mechanics that applies on these states
     * and produce events for presenters to switch screens.
     */

    interface ILevel : IInit, IClean, IStart
    {
        event Action OnVictory;
        event Action OnGameOver;
        event Action OnStart;
        void Unload();
    }

    sealed class Level : ILevel
    {
        [Inject] readonly IArmy _army;
        [Inject] readonly ILevelLoader _loader;
        [Inject] readonly IPlayer _player;
        
        public event Action OnVictory;
        public event Action OnGameOver;
        public event Action OnStart;

        void IInit.Init()
        {
            _player.OnDie += HandlePlayerOnDie;
            _army.OnAllEnemiesDied += HandleOnAllEnemiesDied;
            _army.OnInvadeCompleted += HandleOnInvadeCompleted;
        }

        void IClean.Clean()
        {
            _player.OnDie -= HandlePlayerOnDie;
            _army.OnAllEnemiesDied -= HandleOnAllEnemiesDied;
            _army.OnInvadeCompleted -= HandleOnInvadeCompleted;
        }

        void IStart.Start() => OnStart?.Invoke();

        void ILevel.Unload() => _loader.Unload();

        void HandlePlayerOnDie() => OnGameOver?.Invoke();

        void HandleOnAllEnemiesDied() => OnVictory?.Invoke();

        void HandleOnInvadeCompleted() => OnGameOver?.Invoke();
    }
}