using UnityEngine;

namespace Mechanics.LevelLogic
{
    [CreateAssetMenu(fileName = "NewLevel", menuName = "Level Type", order = 51)]
    sealed class LevelType : ScriptableObject
    {
        public GameObject LevelPrefab;
    }
}