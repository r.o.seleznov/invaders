﻿using Mechanics.ProjectileLogic;
using UnityEngine;

namespace Mechanics.GunLogic
{
    interface IGun
    {
        void Fire();
    }

    sealed class Gun : MonoBehaviour, IGun
    {
        public ProjectileSpawnEvent OnProjectileSpawn;

        [SerializeField] Transform Transform;
        [SerializeField] ProjectileType Type;

        void OnDrawGizmos()
        {
            if (Type == null || Transform == null)
                return;

            var position = Transform.position;

            var spriteRenderer = Type.Prefab.GetComponent<SpriteRenderer>();
            if (spriteRenderer != null)
                Gizmos.color = spriteRenderer.color;

            Gizmos.DrawLine(position, position + Transform.up);
        }

        public void Fire() => OnProjectileSpawn?.Invoke(Request());

        ProjectileSpawnRequest Request() => new(Type, Transform.position, Transform.up);
    }
}