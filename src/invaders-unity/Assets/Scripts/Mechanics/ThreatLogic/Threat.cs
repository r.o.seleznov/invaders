using System;
using Core.Application.LifeCycle;
using Mechanics.ArmyLogic;
using Mechanics.EnemyLogic;
using Zenject;

namespace Mechanics.ThreatLogic
{
    /*
     * This abstraction responds about how enemies behave when we kill their mates.
     * They speed up and become more aggressive. It works on scriptable objects configs.
     */

    interface IThreat : IInit, IClean, IStart
    {
        event Action<ThreatLevelType> OnChanged;
        ThreatLevelType CurrentLevel { get; }
    }

    sealed class Threat : IThreat
    {
        [Inject] readonly IArmy _army;

        public event Action<ThreatLevelType> OnChanged;

        public ThreatLevelType CurrentLevel => _threatLevels[_currentIndex];

        readonly ThreatLevelType[] _threatLevels;
        int _currentIndex;

        public Threat(ThreatType type) => _threatLevels = type.Levels;

        void IInit.Init() => _army.OnEnemyDied += HandleOnEnemyDied;

        void IClean.Clean()
        {
            _currentIndex = 0;
            _army.OnEnemyDied -= HandleOnEnemyDied;
        }

        void IStart.Start() => InvokeOnChange();

        void HandleOnEnemyDied(IEnemy enemy)
        {
            if (ShouldIncrease())
                Increase();
        }

        bool ShouldIncrease()
        {
            bool hasNext = _currentIndex < _threatLevels.Length - 1;
            bool suitableEnemiesCount = _army.EnemiesCount <= CurrentLevel.EnemiesLeft;
            return hasNext && suitableEnemiesCount;
        }

        void Increase()
        {
            _currentIndex++;
            InvokeOnChange();
        }

        void InvokeOnChange() => OnChanged?.Invoke(CurrentLevel);
    }
}