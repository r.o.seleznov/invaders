using UnityEngine;

namespace Mechanics.ThreatLogic
{
    [CreateAssetMenu(fileName = "NewThreat", menuName = "Threat Type", order = 51)]
    class ThreatType : ScriptableObject
    {
        public ThreatLevelType[] Levels;
    }
}