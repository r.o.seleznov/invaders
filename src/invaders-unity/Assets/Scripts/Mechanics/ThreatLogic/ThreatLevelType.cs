using UnityEngine;

namespace Mechanics.ThreatLogic
{
    [CreateAssetMenu(fileName = "NewThreatLevel", menuName = "ThreatLevel Type", order = 51)]
    class ThreatLevelType : ScriptableObject
    {
        public int EnemiesLeft;
    }
}