﻿using System;
using UnityEngine;

namespace Mechanics.ShieldLogic
{
    sealed class Shield : MonoBehaviour
    {
        int _peacesLeft;
        ShieldPiece[] _pieces;

        void Start()
        {
            _pieces = GetComponentsInChildren<ShieldPiece>();
            _peacesLeft = _pieces.Length;

            SubscribeOnCollapse();
        }

        void SubscribeOnCollapse()
        {
            Action onCollapseHandler = HandleOnCollapse;

            foreach (var piece in _pieces)
                piece.OnCollapse += onCollapseHandler;
        }

        void HandleOnCollapse()
        {
            if (--_peacesLeft <= 0)
                Destroy(gameObject);
        }
    }
}