using System;
using UnityEngine;

namespace Mechanics.ShieldLogic
{
    sealed class ShieldPiece : MonoBehaviour
    {
        BoxCollider2D _collider;
        SpriteRenderer _renderer;

        public event Action OnCollapse;

        void Awake()
        {
            _renderer = GetComponent<SpriteRenderer>();
            _collider = GetComponent<BoxCollider2D>();
        }

        void OnTriggerEnter2D(Collider2D other) => Collapse();

        void Collapse()
        {
            _collider.enabled = false;
            _renderer.enabled = true;

            OnCollapse?.Invoke();
        }
    }
}