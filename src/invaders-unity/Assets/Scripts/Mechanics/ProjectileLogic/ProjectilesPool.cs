using System;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Mechanics.ProjectileLogic
{
    /*
     * This is a pool for projectiles. Pool of pools to be correctly.
     * It uses scriptable object ProjectileType as an id.
     * So when a game designer comes to us and says the we wants more kinds of bullets
     * we will just create new assets and there are will pools for new bullets automatically.
     */
    sealed class ProjectilesPool
    {
        readonly GameObject _parent;
        readonly Dictionary<ProjectileType, ProjectilePool> _pools = new();

        public ProjectilesPool(GameObject parent) => _parent = parent;

        public bool Return(Projectile projectile) => Pool(projectile.Type).Return(projectile);

        public Projectile Get(ProjectileType projectileType) => Pool(projectileType).Get();

        public void Cleanup()
        {
            foreach (var pool in _pools.Values)
            {
                pool.OnGet -= PrepareForUsage;
                pool.OnReturn -= PrepareForPool;
            }
        }

        ProjectilePool Pool(ProjectileType projectileType)
        {
            if (!_pools.TryGetValue(projectileType, out var pool))
                pool = CreatePoolFor(projectileType);

            return pool;
        }

        ProjectilePool CreatePoolFor(ProjectileType projectileType)
        {
            var pool = new ProjectilePool(() => Projectile.Create(projectileType, _parent.transform));
            pool.OnGet += PrepareForUsage;
            pool.OnReturn += PrepareForPool;

            _pools[projectileType] = pool;
            return pool;
        }

        static void PrepareForUsage(Projectile projectile) => projectile.gameObject.SetActive(true);

        static void PrepareForPool(Projectile projectile) => projectile.gameObject.SetActive(false);

        // --------------------------------------------------------

        sealed class ProjectilePool : ObjectPool<Projectile>
        {
            public ProjectilePool(Func<Projectile> createNew) : base(createNew) { }
        }
    }
}