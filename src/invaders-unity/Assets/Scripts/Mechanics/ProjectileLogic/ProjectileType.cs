using UnityEngine;

namespace Mechanics.ProjectileLogic
{
    [CreateAssetMenu(fileName = "NewProjectile", menuName = "Projectile", order = 51)]
    public class ProjectileType : ScriptableObject
    {
        public GameObject Prefab;
        public float Speed;
    }
}