﻿using UnityEngine;

namespace Mechanics.ProjectileLogic
{
    public class ProjectileMovement : MonoBehaviour
    {
        Transform _transform;
        Vector3 _direction;
        Vector3 _velocity;
        float _speed;

        public float Speed
        {
            set
            {
                _speed = value;
                UpdateVelocity();
            }
        }

        public Vector3 Direction
        {
            set
            {
                _direction = value;
                UpdateVelocity();
            }
        }

        void Start() => _transform = transform;

        void FixedUpdate() => _transform.position += Time.fixedDeltaTime * _velocity;

        void UpdateVelocity() => _velocity = _speed * _direction;
    }
}