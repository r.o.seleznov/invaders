using System;
using UnityEngine;

namespace Mechanics.ProjectileLogic
{
    sealed class Projectile : MonoBehaviour
    {
        public ProjectileType Type;
        public ProjectileMovement Movement;

        void OnTriggerEnter2D(Collider2D other) => OnCrash?.Invoke(this);

        public static Projectile Create(ProjectileType type, Transform parent)
        {
            var gameObject = Instantiate(type.Prefab, parent);
            var projectile = gameObject.GetComponent<Projectile>();
            projectile.Movement.Speed = type.Speed;
            return projectile;
        }

        public event Action<Projectile> OnCrash;
    }
}