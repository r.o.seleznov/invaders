﻿using System;
using Core.Log;
using UnityEngine;
using Zenject;

namespace Mechanics.ProjectileLogic
{
    /*
     * This is a abstractions where all projectiles live.
     * It works with the help of pool so this is a zero-waste projectile technique.
     */

    sealed class Projectiles : MonoBehaviour
    {
        [Inject] ILog _log;
        
        Action<Projectile> _handleOnCrashDelegate;

        ProjectilesPool _projectilesPool;

        void Awake()
        {
            _projectilesPool = new ProjectilesPool(gameObject);
            _handleOnCrashDelegate = HandleOnCrash;
        }

        void OnDestroy() => _projectilesPool.Cleanup();

        public void HandleOnProjectileSpawn(ProjectileSpawnRequest request) => 
            CreateNext(request.Type, request.Position, request.Direction);

        void CreateNext(ProjectileType type, Vector3 position, Vector3 direction)
        {
            var next = _projectilesPool.Get(type);
            next.transform.position = position;
            next.Movement.Direction = direction;

            SubscribeOn(next);
        }

        void SubscribeOn(Projectile projectile) => projectile.OnCrash += _handleOnCrashDelegate;

        void UnsubscribeFrom(Projectile projectile) => projectile.OnCrash -= _handleOnCrashDelegate;

        void HandleOnCrash(Projectile projectile) => ReturnIntoPool(projectile);

        void ReturnIntoPool(Projectile projectile)
        {
            UnsubscribeFrom(projectile);

            if (!_projectilesPool.Return(projectile))
                _log.Error($"Can not return {projectile.gameObject.name} obstacle to pool");
        }
    }
}