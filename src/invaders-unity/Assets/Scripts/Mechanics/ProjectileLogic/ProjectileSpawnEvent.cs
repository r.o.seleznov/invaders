using System;
using UnityEngine;
using UnityEngine.Events;

namespace Mechanics.ProjectileLogic
{
    [Serializable] sealed class ProjectileSpawnEvent : UnityEvent<ProjectileSpawnRequest> { }

    class ProjectileSpawnRequest
    {
        public ProjectileType Type { get; }
        public Vector3 Position { get; }
        public Vector3 Direction { get; }

        public ProjectileSpawnRequest(ProjectileType type, Vector3 position, Vector3 direction) => 
            (Type, Position, Direction) = (type, position, direction);
    }
}