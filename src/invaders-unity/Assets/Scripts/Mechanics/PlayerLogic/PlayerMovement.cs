﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Mechanics.PlayerLogic
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] float Speed;

        Transform _transform;
        Rigidbody2D _rigidbody;
        Vector3 _speed;
        Vector2 _input;

        void Start()
        {
            _transform = transform;
            _rigidbody = GetComponent<Rigidbody2D>();
            _speed = Speed * Vector3.right;
        }

        void FixedUpdate()
        {
            var position = _transform.position + Time.fixedDeltaTime * _input.x * _speed;
            _rigidbody.MovePosition(position);
        }

        public void Input(InputAction.CallbackContext context) => _input = context.ReadValue<Vector2>();
    }
}