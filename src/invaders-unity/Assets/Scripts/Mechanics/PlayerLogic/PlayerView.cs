﻿using System;
using System.Collections;
using Core.Mvp;
using JetBrains.Annotations;
using Mechanics.GunLogic;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils;
using Zenject;

namespace Mechanics.PlayerLogic
{
    interface IPlayerView : IView
    {
        event Action OnProjectileCollide;
    }

    sealed class PlayerView : View, IPlayerView
    {
        static readonly AnimId IsAliveId = "IsAlive";

        [Inject] PlayerPresenter.Factory _factory;
        protected override IPresenter NewPresenter => _factory.Create(this);
        
        [SerializeField] PlayerMovement Movement;
        [SerializeField] Collider2D Collider;
        [SerializeField] Animator Animator;
        [SerializeField] Gun Gun;
        
        public event Action OnProjectileCollide;

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.layer == Layer.EnemyProjectile)
                StartCoroutine(DieRoutine());
        }

        [UsedImplicitly]
        public void Fire(InputAction.CallbackContext context)
        {
            if (context.performed)
                Gun.Fire();
        }

        IEnumerator DieRoutine()
        {
            Collider.enabled = false;
            Movement.enabled = false;
            Animator.SetBool(IsAliveId, false);
            yield return new WaitForSeconds(0.5f);
            OnProjectileCollide?.Invoke();
        }
    }
}