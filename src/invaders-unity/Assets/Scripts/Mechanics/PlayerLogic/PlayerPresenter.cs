using Core.Mvp;
using Zenject;

namespace Mechanics.PlayerLogic
{
    sealed class PlayerPresenter : Presenter<IPlayerView>
    {
        public PlayerPresenter(IPlayerView view) : base(view) { }

        protected override void Subscribe() => View.OnProjectileCollide += HandleOnProjectileCollide;

        protected override void Unsubscribe() => View.OnProjectileCollide -= HandleOnProjectileCollide;

        void HandleOnProjectileCollide() => Model.Player.TakeDamage();

        // ------------------------------
        
        public sealed class Factory : PlaceholderFactory<IPlayerView, PlayerPresenter> { }
    }
}