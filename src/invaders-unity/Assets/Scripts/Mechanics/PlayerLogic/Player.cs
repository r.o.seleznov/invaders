using System;

namespace Mechanics.PlayerLogic
{
    interface IPlayer
    {
        event Action OnDie;
        void TakeDamage();
    }

    sealed class Player : IPlayer
    {
        public event Action OnDie;

        void IPlayer.TakeDamage() => OnDie?.Invoke();
    }
}