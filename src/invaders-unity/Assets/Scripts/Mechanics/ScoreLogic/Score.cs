using System;
using Core.Application.LifeCycle;
using Mechanics.ArmyLogic;
using Mechanics.EnemyLogic;
using Mechanics.UfoLogic;
using Zenject;

namespace Mechanics.ScoreLogic
{
    interface IScore : IInit, IClean
    {
        event Action<int> OnChanged;
        int Value { get; }
    }

    sealed class Score : IScore
    {
        [Inject] readonly IArmy _army;
        [Inject] readonly IUfo _ufo;

        public event Action<int> OnChanged;
        
        public int Value
        {
            get => _value;
            private set
            {
                _value = value;
                OnChanged?.Invoke(_value);
            }
        }

        int _value;

        void IInit.Init()
        {
            _army.OnEnemyDied += HandleOnEnemyDied;
            _ufo.OnDie += HandleOnUfoDie;
        }

        void IClean.Clean()
        {
            _value = 0;
            _army.OnEnemyDied -= HandleOnEnemyDied;
            _ufo.OnDie -= HandleOnUfoDie;
        }

        void HandleOnEnemyDied(IEnemy enemy) => Value += enemy.ScorePoints;

        void HandleOnUfoDie(IUfo ufo) => Value += ufo.Data.ScorePoints;
    }
}