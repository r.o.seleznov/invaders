﻿using System;
using UnityEngine;
using Utils;

namespace Mechanics.EnemyLogic
{
    interface IEnemyMovement
    {
        float Speed { set; }
        void MoveDown();
        void TurnAround();
        void Stop();
        void ApplyLevel(EnemyType type, EnemyType.EnemyLevel level);
    }

    [RequireComponent(typeof(Animator))]
    sealed class EnemyMovement : MonoBehaviour, IEnemyMovement
    {
        static readonly AnimId SpeedMultiplierId = "SpeedMultiplier";

        public float Speed { get => _velocity.x; set => _velocity = value * Vector3.right; }
        
        Transform _transform;
        Animator _animator;
        Vector3 _velocity;

        void Awake()
        {
            _transform = transform;
            _animator = GetComponent<Animator>();
        }

        void FixedUpdate() => _transform.position += Time.fixedDeltaTime * _velocity;

        public void MoveDown() => _transform.position += 0.1f * Vector3.down;

        public void TurnAround() => Speed *= -1;

        public void Stop() => enabled = false;

        public void ApplyLevel(EnemyType type, EnemyType.EnemyLevel level)
        {
            _animator.SetFloat(SpeedMultiplierId, level.SpeedMultiplier);
            Speed = level.SpeedMultiplier * type.Speed * Math.Sign(Speed);
        }
    }
}