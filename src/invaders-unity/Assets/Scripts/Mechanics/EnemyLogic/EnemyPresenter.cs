using Core.Mvp;
using Mechanics.ThreatLogic;
using Zenject;

namespace Mechanics.EnemyLogic
{
    sealed class EnemyPresenter : Presenter<IEnemyView>
    {
        public EnemyPresenter(IEnemyView view) : base(view) { }

        protected override void Subscribe()
        {
            Model.Level.OnStart += HandleLevelOnStart;
            Model.Army.OnEnemyDied += HandleOnEnemyDied;
            Model.Threat.OnChanged += HandleOnThreatChanged;
            Model.ArmyAttack.OnAttack += HandleOnAttack;
            View.OnProjectileCollide += HandleOnProjectileCollide;
        }

        protected override void Unsubscribe()
        {
            Model.Level.OnStart -= HandleLevelOnStart;
            Model.Army.OnEnemyDied -= HandleOnEnemyDied;
            Model.Threat.OnChanged -= HandleOnThreatChanged;
            Model.ArmyAttack.OnAttack -= HandleOnAttack;
            View.OnProjectileCollide -= HandleOnProjectileCollide;
        }

        void HandleOnEnemyDied(IEnemy enemy)
        {
            if (View.Id == enemy.Id)
                View.Die();
        }

        void HandleOnAttack(uint enemyId)
        {
            if (View.Id == enemyId)
                View.Gun.Fire();
        }

        void HandleLevelOnStart() => View.ApplyThreatLevel(Model.Threat.CurrentLevel);

        void HandleOnThreatChanged(ThreatLevelType levelType) => View.ApplyThreatLevel(levelType);

        void HandleOnProjectileCollide(IEnemyView enemy) => Model.Army.TakeDamage(enemy.Id);

        // ------------------------------
        
        public sealed class Factory : PlaceholderFactory<IEnemyView, EnemyPresenter> { }
    }
}