﻿using System;
using System.Collections;
using Core.Mvp;
using Mechanics.GunLogic;
using Mechanics.ThreatLogic;
using UnityEngine;
using Utils;
using Zenject;

namespace Mechanics.EnemyLogic
{
    interface IEnemyView : IView
    {
        event Action OnWallCollide;
        event Action OnOrbitCollide;
        event Action<IEnemyView> OnProjectileCollide;
        
        uint Id { get; }
        IEnemyMovement Movement { get; }
        IGun Gun { get; }
        int ScorePoints { get; }

        void Die();
        void ApplyThreatLevel(ThreatLevelType threatLevel);
    }

    sealed class EnemyView : View, IEnemyView
    {
        static readonly AnimId IsAliveId = "IsAlive";
        static readonly YieldInstruction WaitForDeath = new WaitForSeconds(0.5f);
        
        [Inject] EnemyPresenter.Factory _factory;
        protected override IPresenter NewPresenter => _factory.Create(this);

        [SerializeField] Animator Animator;
        [SerializeField] EnemyType Type;
        [SerializeField] Collider2D Collider;
        
        public event Action OnWallCollide;
        public event Action OnOrbitCollide;
        public event Action<IEnemyView> OnProjectileCollide;

        public uint Id { get; set; }
        public IEnemyMovement Movement { get; private set; }
        public IGun Gun { get; private set; }
        public int ScorePoints => Type.ScorePoints;
        
        EnemyType.EnemyLevel _currentLevel;

        void OnTriggerEnter2D(Collider2D other)
        {
            int layer = other.gameObject.layer;

            if (layer == Layer.Wall)
                OnWallCollide?.Invoke();

            else if (layer == Layer.WallOrbit || layer == Layer.Enemy)
                OnOrbitCollide?.Invoke();

            else if (layer == Layer.PlayerProjectile)
                OnProjectileCollide?.Invoke(this);
        }

        public void Die()
        {
            Collider.enabled = false;
            Movement.Stop();
            Animator.SetBool(IsAliveId, false);
            StartCoroutine(DieRoutine());
        }

        public void ApplyThreatLevel(ThreatLevelType threatLevel)
        {
            var enemyLevel = EnemyLevelFor(threatLevel);
            if (enemyLevel == _currentLevel)
                return;

            _currentLevel = enemyLevel;
            Movement.ApplyLevel(Type, _currentLevel);
        }

        protected override void OnAwake()
        {
            Movement = gameObject.GetComponentInChildrenOrAdd<EnemyMovement>();
            Movement.Speed = Type.Speed;

            Gun = gameObject.GetComponentInChildren<Gun>();
        }

        EnemyType.EnemyLevel EnemyLevelFor(ThreatLevelType threatLevel)
        {
            foreach (var level in Type.Levels)
                if (level.ThreatLevel == threatLevel)
                    return level;

            return Type.Default;
        }

        IEnumerator DieRoutine()
        {
            yield return WaitForDeath;
            Destroy(gameObject);
        }

        public override string ToString() =>
            $"Id = {Id} | " +
            $"Name = {name} | " +
            $"Type = {Type.name}";
    }
}