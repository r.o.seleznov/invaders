using System;
using Mechanics.ThreatLogic;
using UnityEngine;

namespace Mechanics.EnemyLogic
{
    [CreateAssetMenu(fileName = "NewEnemy", menuName = "Enemy Type", order = 51)]
    class EnemyType : ScriptableObject
    {
        public float Speed;
        public int ScorePoints;
        public EnemyLevel Default;
        public EnemyLevel[] Levels;

        [Serializable]
        public class EnemyLevel
        {
            public ThreatLevelType ThreatLevel;
            public float SpeedMultiplier;
        }
    }
}