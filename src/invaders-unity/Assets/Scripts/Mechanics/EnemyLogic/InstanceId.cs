namespace Mechanics.EnemyLogic
{
    static class InstanceId<T>
    {
        // ReSharper disable once StaticMemberInGenericType

        /*
         * That's what we need: for each type of T the _id will be different. Just a simple counter
         */

        static uint _id;
        public static uint Next => ++_id;
    }
}