namespace Mechanics.EnemyLogic
{
    /*
     * Model part of an enemy.
     */

    interface IEnemy
    {
        uint Id { get; }
        string Name { get; }
        int ScorePoints { get; }
    }

    sealed class Enemy : IEnemy
    {
        public Enemy(uint id, string name, int scorePoints)
        {
            (Id, Name, ScorePoints) = (id, name, scorePoints);
        }

        public uint Id { get; }
        public string Name { get; }
        public int ScorePoints { get; }

        /*
         * Highly useful approach to debug types is to override its ToString method.
         */
        public override string ToString() =>
            $"{nameof(Id)} = {Id} | " +
            $"{nameof(Name)} = {Name} | " +
            $"{nameof(ScorePoints)} = {ScorePoints}";
    }
}