using Core.Mvp;
using Mechanics.EnemyLogic;
using Zenject;

namespace Mechanics.ArmyLogic
{
    class ArmyPresenter : Presenter<IArmyView>
    {
        public ArmyPresenter(IArmyView view) : base(view) { }

        protected override void Subscribe()
        {
            Model.Army.OnEnemyDied += HandleOnEnemyDie;
            View.Movement.OnOrbitCollide += HandleOnOrbitCollide;
        }

        protected override void Unsubscribe()
        {
            Model.Army.OnEnemyDied -= HandleOnEnemyDie;
            View.Movement.OnOrbitCollide -= HandleOnOrbitCollide;
        }

        void HandleOnEnemyDie(IEnemy enemy) => View.RemoveEnemy(enemy.Id);

        void HandleOnOrbitCollide() => Model.Army.CompleteInvade();

        // ------------------------------
        
        public sealed class Factory : PlaceholderFactory<IArmyView, ArmyPresenter> { }
    }
}