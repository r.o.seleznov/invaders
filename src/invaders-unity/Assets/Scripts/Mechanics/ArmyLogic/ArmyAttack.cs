using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Application.LifeCycle;
using Core.Log;
using Mechanics.EnemyLogic;
using Mechanics.ThreatLogic;
using UnityEngine;
using Utils;
using Zenject;
using Random = UnityEngine.Random;

namespace Mechanics.ArmyLogic
{
    /*
     * This abstraction responds about how army attack.
     * There is a timer here which ticks and calls attack on a random enemy
     * from the bottom of army. So here we implements a feature when only opened invaders can fire.
     *
     * In ideal case the timer and the random in this class have to be implemented through .net
     * to get Unity-free code. But a built-in System.Timers.Timer works on separate thread.
     * So it should be some custom thread management system to handle this.
     * For development speed purpose it works now on Unity.
     */

    interface IArmyAttack : IInit, IClean
    {
        event Action<uint> OnAttack;
    }

    sealed class ArmyAttack : IArmyAttack
    {
        [Inject] readonly ILog _log;
        [Inject] readonly IArmy _army;
        [Inject] readonly ICoroutines _coroutines;
        [Inject] readonly IThreat _threat;

        public event Action<uint> OnAttack;
        
        readonly ArmyAttackData.ArmyLevel[] _levels;

        Coroutine _attackCoroutine;
        YieldInstruction _attackTimeout;
        ArmyAttackData.ArmyLevel _currentLevel;

        public ArmyAttack(ArmyAttackType attackType) => _levels = attackType.Data.Levels;

        void IInit.Init() => _threat.OnChanged += HandleThreatOnChange;

        void IClean.Clean()
        {
            _threat.OnChanged -= HandleThreatOnChange;
            StopFireTimer();
        }

        void HandleThreatOnChange(ThreatLevelType threatLevel) => Apply(threatLevel);

        void Apply(ThreatLevelType threatLevel)
        {
            var armyLevel = ArmyLevelFor(threatLevel);
            if (armyLevel == null)
            {
                _log.Info($"Can not find army level for threat: {threatLevel}");
                return;
            }

            if (_currentLevel == armyLevel)
                return;

            _currentLevel = armyLevel;

            Attack(_currentLevel);

            _log.Info(_currentLevel.ToString());
        }

        ArmyAttackData.ArmyLevel ArmyLevelFor(ThreatLevelType threatLevel) => 
            _levels.FirstOrDefault(x => x.ThreatLevel == threatLevel);

        void Attack(ArmyAttackData.ArmyLevel level)
        {
            SetupTimeout(level);
            StartFireTimer();
        }

        void SetupTimeout(ArmyAttackData.ArmyLevel level) => 
            _attackTimeout = new WaitForSeconds(level.FireTimeoutInSeconds);

        void StartFireTimer() => _attackCoroutine = _coroutines.Start(FireTimer());

        void StopFireTimer() => _coroutines.Stop(ref _attackCoroutine);

        IEnumerator FireTimer()
        {
            yield return _attackTimeout;

            if (PassProbability())
                OnAttack?.Invoke(RandomEnemy());

            StartFireTimer();
        }

        bool PassProbability() => Random.value < _currentLevel.FireProbability;

        uint RandomEnemy()
        {
            var column = RandomNotEmptyColumn();
            return column == null ? uint.MaxValue : column[^1].Id;
        }

        IReadOnlyList<IEnemy> RandomNotEmptyColumn()
        {
            if (_army.Empty())
                return null;

            var enemies = _army.Enemies;

            while (true)
            {
                int index = Random.Range(0, enemies.Count);
                var column = enemies[index];
                if (column.Count > 0)
                    return column;
            }
        }
    }
}