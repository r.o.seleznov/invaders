﻿using System.Collections.Generic;
using Core.Mvp;
using Mechanics.EnemyLogic;
using Utils;
using Zenject;

namespace Mechanics.ArmyLogic
{
    /*
     * As Army class is a container for enemies model parts this ArmyView class is
     * a container for enemies view parts. These containers synchronize through ArmyPresenter
     * using enemy ids.
     */

    interface IArmyView : IView
    {
        IEnumerable<IEnemyView> Enemies { get; }
        IArmyMovement Movement { get; }
        void RemoveEnemy(uint enemyId);
    }

    sealed class ArmyView : View, IArmyView
    {
        readonly List<IEnemyView> _enemies = new(100);
        
        public IEnumerable<IEnemyView> Enemies => _enemies;
        
        public IArmyMovement Movement { get; private set; }
        
        [Inject] ArmyPresenter.Factory _factory;
        protected override IPresenter NewPresenter => _factory.Create(this);

        public void RemoveEnemy(uint enemyId) => _enemies.RemoveFirst(x => x.Id == enemyId);

        protected override void OnAwake()
        {
            Movement = GetComponent<IArmyMovement>();
            InitializeEnemies();
        }

        void InitializeEnemies()
        {
            var enemies = GetComponentsInChildren<IEnemyView>();

            foreach (var enemy in enemies)
                _enemies.Add(enemy);
        }
    }
}