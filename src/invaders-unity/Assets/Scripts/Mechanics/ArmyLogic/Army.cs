using System;
using System.Collections.Generic;
using System.Linq;
using Core.Application.LifeCycle;
using Core.Application.Loaders;
using Mechanics.EnemyLogic;
using Utils;
using Zenject;

namespace Mechanics.ArmyLogic
{
    /*
     * This model abstraction is like a register or container of enemies in model.
     * It knows how many enemies alive and notifies mechanics and presenters about changes in them.
     */

    interface IArmy : IStart
    {
        event Action<IEnemy> OnEnemyDied;
        event Action OnAllEnemiesDied;
        event Action OnInvadeCompleted;
        
        IReadOnlyList<IReadOnlyList<IEnemy>> Enemies { get; }
        int EnemiesCount { get; }

        void TakeDamage(uint enemyId);
        void CompleteInvade();
    }

    class Army : IArmy
    {
        [Inject] IEnemiesLoader _loader;

        public event Action<IEnemy> OnEnemyDied;
        public event Action OnAllEnemiesDied;
        public event Action OnInvadeCompleted;
        
        public IReadOnlyList<IReadOnlyList<IEnemy>> Enemies => _enemies;
        public int EnemiesCount { get; private set; }
        
        List<IEnemy>[] _enemies;

        public Army(IEnemiesLoader loader) => _loader = loader;

        void IStart.Start()
        {
            _enemies = _loader.Load();
            EnemiesCount = _enemies.Sum(x => x.Count);
        }

        public void TakeDamage(uint enemyId)
        {
            var died = Remove(enemyId);
            if (died == null)
                return;

            if (--EnemiesCount == 0)
                OnAllEnemiesDied?.Invoke();

            OnEnemyDied?.Invoke(died);
        }

        public void CompleteInvade() => OnInvadeCompleted?.Invoke();

        IEnemy Remove(uint id)
        {
            foreach (var column in _enemies)
            {
                var removed = column.RemoveFirst(x => x.Id == id);
                if (removed != null)
                    return removed;
            }

            return null;
        }
    }
}