namespace Mechanics.ArmyLogic
{
    static class ArmyExtensions
    {
        public static bool Empty(this IArmy army) => army.EnemiesCount == 0;
    }
}