using System;
using System.Collections;
using UnityEngine;

namespace Mechanics.ArmyLogic
{
    interface IArmyMovement
    {
        event Action OnOrbitCollide;
    }

    /*
     * Here we handle how army collides with screen borders and moves down.
    */

    [RequireComponent(typeof(ArmyView))]
    sealed class ArmyMovement : MonoBehaviour, IArmyMovement
    {
        IArmyView _armyView;
        bool _ignoringNow;

        void Awake() => _armyView = GetComponent<IArmyView>();

        void Start()
        {
            foreach (var enemy in _armyView.Enemies)
            {
                enemy.OnWallCollide += HandleOnWallCollide;
                enemy.OnOrbitCollide += HandleOnOrbitCollide;
            }
        }

        public event Action OnOrbitCollide;

        void HandleOnOrbitCollide()
        {
            if (Ignore())
                return;

            OnOrbitCollide?.Invoke();
        }

        void HandleOnWallCollide()
        {
            if (Ignore())
                return;

            TurnAroundAndMoveDown();
        }

        bool Ignore()
        {
            if (_ignoringNow)
                return true;

            IgnoreCollisionsForAWhile();
            return false;
        }

        void TurnAroundAndMoveDown()
        {
            foreach (var enemy in _armyView.Enemies)
            {
                var movement = enemy.Movement;
                movement.TurnAround();
                movement.MoveDown();
            }
        }

        void IgnoreCollisionsForAWhile() => StartCoroutine(IgnoreRoutine());

        IEnumerator IgnoreRoutine()
        {
            _ignoringNow = true;
            yield return null;
            _ignoringNow = false;
        }
    }
}