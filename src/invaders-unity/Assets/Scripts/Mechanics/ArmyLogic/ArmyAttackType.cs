using System;
using Mechanics.ThreatLogic;
using UnityEngine;

namespace Mechanics.ArmyLogic
{
    [CreateAssetMenu(fileName = "NewArmyAttack", menuName = "Army Attack Type", order = 51)]
    class ArmyAttackType : ScriptableObject
    {
        public ArmyAttackData Data;
    }

    [Serializable]
    class ArmyAttackData
    {
        public ArmyLevel[] Levels;

        [Serializable]
        public class ArmyLevel
        {
            public ThreatLevelType ThreatLevel;
            public float FireTimeoutInSeconds;
            [Range(0f, 1f)] public float FireProbability;

            public override string ToString() =>
                $"Level: {ThreatLevel.name} | " +
                $"Timeout: {FireTimeoutInSeconds} | " +
                $"Probability: {FireProbability}";
        }
    }
}