using Core.Mvp;
using Zenject;

namespace Mechanics.UfoLogic
{
    sealed class UfoPresenter : Presenter<IUfoView>
    {
        public UfoPresenter(IUfoView view) : base(view) { }

        protected override void Subscribe()
        {
            Model.Ufo.OnSpawn += HandleOnSpawn;
            Model.Ufo.OnDie += HandleOnDie;
            Model.Level.OnVictory += HandleLevelOnVictory;
            View.OnProjectileCollide += HandleOnProjectileCollide;
        }

        protected override void Unsubscribe()
        {
            Model.Ufo.OnSpawn -= HandleOnSpawn;
            Model.Ufo.OnDie -= HandleOnDie;
            Model.Level.OnVictory -= HandleLevelOnVictory;
            View.OnProjectileCollide -= HandleOnProjectileCollide;
        }

        void HandleOnSpawn() => View.Spawn.Call();

        void HandleOnDie(IUfo ufo) => View.Die();

        void HandleLevelOnVictory() => View.Movement.Stop();

        void HandleOnProjectileCollide(IUfoView ufo) => Model.Ufo.TakeDamage();

        public sealed class Factory : PlaceholderFactory<IUfoView, UfoPresenter> { }
    }
}