using System;
using System.Collections;
using Core.Mvp;
using UnityEngine;
using Utils;
using Zenject;

namespace Mechanics.UfoLogic
{
    interface IUfoView : IView
    {
        event Action<IUfoView> OnProjectileCollide;
        IUfoMovement Movement { get; }
        IUfoSpawn Spawn { get; }
        void Die();
    }

    sealed class UfoView : View, IUfoView
    {
        static readonly AnimId IsAliveId = "IsAlive";

        [SerializeField] Animator Animator;
        [SerializeField] UfoType Type;
        [SerializeField] Collider2D Collider;

        public event Action<IUfoView> OnProjectileCollide;
        
        [Inject] UfoPresenter.Factory _factory;
        protected override IPresenter NewPresenter => _factory.Create(this);

        void OnTriggerEnter2D(Collider2D other)
        {
            int layer = other.gameObject.layer;

            if (layer == Layer.PlayerProjectile)
                OnProjectileCollide?.Invoke(this);

            else if (layer == Layer.WallUfo)
                Disappear();
        }

        public IUfoMovement Movement { get; private set; }
        
        public IUfoSpawn Spawn { get; private set; }

        public void Die()
        {
            Collider.enabled = false;
            Movement.Stop();
            Animator.SetBool(IsAliveId, false);
            StartCoroutine(DieRoutine());
        }

        protected override void OnAwake()
        {
            Movement = GetComponent<UfoMovement>();
            Movement.Speed = Type.Data.Speed;
            Spawn = GetComponent<UfoSpawn>();
        }

        public void Appear()
        {
            Collider.enabled = true;
            Movement.Start();
            Animator.SetBool(IsAliveId, true);
            gameObject.SetActive(true);
        }

        void Disappear() => gameObject.SetActive(false);

        IEnumerator DieRoutine()
        {
            yield return new WaitForSeconds(0.5f);
            Disappear();
        }
    }
}