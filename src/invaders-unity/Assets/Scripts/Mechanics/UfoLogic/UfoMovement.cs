using UnityEngine;

namespace Mechanics.UfoLogic
{
    interface IUfoMovement
    {
        float Speed { set; }
        Vector3 Direction { set; }
        void Stop();
        void Start();
    }

    sealed class UfoMovement : MonoBehaviour, IUfoMovement
    {
        Transform _transform;
        Vector3 _direction;
        Vector3 _velocity;
        float _speed;

        void Awake()
        {
            _transform = transform;
            Direction = Vector3.right;
        }

        void FixedUpdate() => _transform.position += Time.fixedDeltaTime * _velocity;

        public float Speed
        {
            set
            {
                _speed = value;
                UpdateVelocity();
            }
        }

        public Vector3 Direction
        {
            set
            {
                _direction = value;
                UpdateVelocity();
            }
        }

        void IUfoMovement.Stop() => enabled = false;

        void IUfoMovement.Start() => enabled = true;

        void UpdateVelocity() => _velocity = _speed * _direction;
    }
}