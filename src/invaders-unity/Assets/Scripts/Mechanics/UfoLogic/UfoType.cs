using System;
using UnityEngine;

namespace Mechanics.UfoLogic
{
    [CreateAssetMenu(fileName = "NewUfo", menuName = "Ufo Type", order = 51)]
    sealed class UfoType : ScriptableObject
    {
        public UfoData Data;
    }

    [Serializable]
    sealed class UfoData
    {
        public float SpawnIntervalInSeconds;
        public float Speed;
        public int ScorePoints;
    }
}