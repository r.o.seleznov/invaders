using System;
using System.Collections;
using Core.Application.LifeCycle;
using UnityEngine;
using Utils;
using Zenject;

namespace Mechanics.UfoLogic
{
    interface IUfo : IClean, IStart
    {
        event Action OnSpawn;
        event Action<IUfo> OnDie;
        UfoData Data { get; }
        void TakeDamage();
    }

    sealed class Ufo : IUfo
    {
        [Inject] readonly ICoroutines _coroutines;
        
        public event Action OnSpawn;
        public event Action<IUfo> OnDie;
        
        public UfoData Data { get; }
        
        YieldInstruction _spawnTimeout;
        Coroutine _timerRoutine;

        public Ufo(UfoType type) => Data = type.Data;

        void IClean.Clean() => StopSpawnTimer();

        void IStart.Start()
        {
            SetupTimeout();
            StartSpawnTimer();
        }

        void IUfo.TakeDamage() => OnDie?.Invoke(this);

        void SetupTimeout() => _spawnTimeout = new WaitForSeconds(Data.SpawnIntervalInSeconds);

        void StartSpawnTimer() => _timerRoutine = _coroutines.Start(SpawnTimer());

        void StopSpawnTimer() => _coroutines.Stop(ref _timerRoutine);

        IEnumerator SpawnTimer()
        {
            yield return _spawnTimeout;
            OnSpawn?.Invoke();
            StartSpawnTimer();
        }
    }
}