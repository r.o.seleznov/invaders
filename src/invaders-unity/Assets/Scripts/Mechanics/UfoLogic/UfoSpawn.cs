using UnityEngine;

namespace Mechanics.UfoLogic
{
    interface IUfoSpawn
    {
        void Call();
    }

    sealed class UfoSpawn : MonoBehaviour, IUfoSpawn
    {
        [SerializeField] UfoView Ufo;
        [SerializeField] Transform LeftSpawnPoint;
        [SerializeField] Transform RightSpawnPoint;

        public void Call()
        {
            bool left = Random.value < 0.5f;
            if (left)
            {
                Ufo.transform.position = LeftSpawnPoint.position;
                Ufo.Movement.Direction = Vector3.right;
            }
            else
            {
                Ufo.transform.position = RightSpawnPoint.position;
                Ufo.Movement.Direction = Vector3.left;
            }

            Ufo.Appear();
        }
    }
}