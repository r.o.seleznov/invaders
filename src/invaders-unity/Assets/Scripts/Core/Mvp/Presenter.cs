using UI;
using Zenject;

namespace Core.Mvp
{
    /*
     * Presenter allow us to bind a communication between View and Model.
     * Here we get a references both to View and Model.
     * As a communication with the view are going through an IView interface
     * we do not have Unity-dependent code in presenters and models.
    */

    interface IPresenter
    {
        void Initialize();
        void Uninitialize();
        void Start();
    }

    abstract class Presenter : IPresenter
    {
        [Inject] protected IModel Model { get; private set; }

        public void Initialize() => Subscribe();
        public void Uninitialize() => Unsubscribe();

        public virtual void Start() { }

        protected virtual void Subscribe() { }
        protected virtual void Unsubscribe() { }
    }

    abstract class Presenter<TView> : Presenter where TView : IView
    {
        protected Presenter(TView view) => View = view;

        protected TView View { get; }
    }

    abstract class ScreenPresenter<T> : Presenter<T> where T : IView
    {
        [Inject] protected IHUD HUD { get; private set; }
        
        protected ScreenPresenter(T view) : base(view) { }
    }
}