using Mechanics.ArmyLogic;
using Mechanics.EnemyLogic;
using Mechanics.PlayerLogic;
using Mechanics.UfoLogic;
using UI.ScreenLogic.GameOver;
using UI.ScreenLogic.InGame;
using UI.ScreenLogic.Start;
using UI.ScreenLogic.Victory;
using Zenject;

namespace Core.Mvp
{
    sealed class ViewInstaller : Installer<ViewInstaller>
    {
        public override void InstallBindings()
        {
            Bind<IUfoView, UfoPresenter, UfoPresenter.Factory>();
            Bind<IArmyView, ArmyPresenter, ArmyPresenter.Factory>();
            Bind<IEnemyView, EnemyPresenter, EnemyPresenter.Factory>();
            Bind<IPlayerView, PlayerPresenter, PlayerPresenter.Factory>();

            Bind<IStartScreen, StartScreenPresenter, StartScreenPresenter.Factory>().AsSingle();
            Bind<IInGameScreen, InGameScreenPresenter, InGameScreenPresenter.Factory>();
            Bind<IVictoryScreen, VictoryScreenPresenter, VictoryScreenPresenter.Factory>();
            Bind<IGameOverScreen, GameOverScreenPresenter, GameOverScreenPresenter.Factory>();
        }
        
        FactoryToChoiceIdBinder<TParam, TContract> Bind<TParam, TContract, TFactory>() 
            where TFactory : PlaceholderFactory<TParam, TContract>
            => Container.BindFactory<TParam, TContract, TFactory>();
    }
}