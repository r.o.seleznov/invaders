using UnityEngine;

namespace Core.Mvp
{
    /*
     * Here is a Unity dependent part of the game.
    */

    interface IView { }

    abstract class View : MonoBehaviour, IView
    {
        IPresenter _presenter;
        protected abstract IPresenter NewPresenter { get; }

        void Awake() => OnAwake();

        void Start()
        {
            _presenter = NewPresenter;
            _presenter.Initialize();
            _presenter.Start();
        }

        void OnDestroy() => _presenter?.Uninitialize();

        protected virtual void OnAwake() { }
    }
}