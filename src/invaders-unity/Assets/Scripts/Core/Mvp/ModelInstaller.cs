using Mechanics.ArmyLogic;
using Mechanics.LevelLogic;
using Mechanics.PlayerLogic;
using Mechanics.ScoreLogic;
using Mechanics.ThreatLogic;
using Mechanics.UfoLogic;
using Zenject;

namespace Core.Mvp
{
    sealed class ModelInstaller : Installer<ModelInstaller>
    {
        public override void InstallBindings()
        {
            Bind<Model>().AsSingle().NonLazy();
            Bind<Army>().AsSingle().NonLazy();
            Bind<Player>().AsSingle().NonLazy();
            Bind<Threat>().AsSingle().NonLazy();
            Bind<ArmyAttack>().AsSingle().NonLazy();
            Bind<Ufo>().AsSingle().NonLazy();
            Bind<Score>().AsSingle().NonLazy();
            Bind<Level>().AsSingle().NonLazy();
        }

        FromBinderNonGeneric Bind<TContract>() => Container.BindInterfacesTo<TContract>();
    }
}