using System;
using Core.Application.LifeCycle;
using Mechanics.ArmyLogic;
using Mechanics.LevelLogic;
using Mechanics.PlayerLogic;
using Mechanics.ScoreLogic;
using Mechanics.ThreatLogic;
using Mechanics.UfoLogic;
using Utils;
using Zenject;

namespace Core.Mvp
{
    /*
     * A Model is a container for game mechanics.
     *
     * IInit, IClean and IStart interfaces give an opportunity to control mechanic's life cycle.
     * As it works through reflection we need just choose needed methods
     * and implement corresponding interface in mechanic class.
     */

    interface IModelLifeCycle : IInit, IClean, IStart { }

    interface IModel : IInitializable, IDisposable, IModelLifeCycle
    {
        IPlayer Player { get; }
        IArmy Army { get; }
        IArmyAttack ArmyAttack { get; }
        IThreat Threat { get; }
        IUfo Ufo { get; }
        IScore Score { get; }
        ILevel Level { get; }
    }

    sealed class Model : IModel
    {
        IInit[] _inits;
        IClean[] _cleans;
        IStart[] _starts;
        
        [Inject] public IPlayer Player { get; private set; }
        [Inject] public IArmy Army { get; private set; }
        [Inject] public IArmyAttack ArmyAttack { get; private set; }
        [Inject] public IThreat Threat { get; private set; }
        [Inject] public IUfo Ufo { get; private set; }
        [Inject] public IScore Score { get; private set; }
        [Inject] public ILevel Level { get; private set; }

        void IInitializable.Initialize()
        {
            _inits = this.PropertiesOfType<IInit>();
            _cleans = this.PropertiesOfType<IClean>();
            _starts = this.PropertiesOfType<IStart>();
        }

        void IDisposable.Dispose() => ((IClean)this).Clean();

        void IInit.Init() => _inits.ForEach(x => x.Init());

        void IClean.Clean() => _cleans.ForEach(x => x.Clean());

        void IStart.Start() => _starts.ForEach(x => x.Start());
    }
}