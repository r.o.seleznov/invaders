using UnityEngine;

namespace Core.Log
{
    interface ILog
    {
        void Info(string msg);
        void Error(string msg);
    }

    sealed class UnityLog : ILog
    {
        public void Info(string msg) => Debug.Log(msg);
        public void Error(string msg) => Debug.LogError(msg);
    }
}