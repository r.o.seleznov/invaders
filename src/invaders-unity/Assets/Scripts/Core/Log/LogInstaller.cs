using Zenject;

namespace Core.Log
{
    sealed class LogInstaller : Installer<LogInstaller>
    {
        public override void InstallBindings() => Container.Bind<ILog>().To<UnityLog>().AsSingle();
    }
}