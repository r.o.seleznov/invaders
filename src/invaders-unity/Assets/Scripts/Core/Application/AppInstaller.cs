using Core.Application.Factories;
using Core.Application.LifeCycle;
using Core.Application.Loaders;
using Core.Log;
using Core.Mvp;
using UI;
using Utils;
using Zenject;

namespace Core.Application
{
    sealed class AppInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Log();
            Coroutines();
            AppLifeCycle();
            Factories();
            Hud();
            Loaders();
            Mvp();
        }

        void Log() => LogInstaller.Install(Container);

        void Coroutines() => Bind<ICoroutines>().FromInstance(new Coroutines(this)).AsSingle();

        void AppLifeCycle() => BindInterfacesTo<AppLifeCycle>().FromComponentsOn(gameObject).AsSingle();

        void Factories() => FactoriesInstaller.Install(Container);

        void Hud() => Bind<IHUD>().To<HUD>().FromComponentsInChildren().AsSingle();

        void Loaders()
        {
            Bind<IEnemiesLoader>().To<EnemiesLoader>().FromComponentsOn(gameObject).AsSingle();
            Bind<ILevelLoader>().To<LevelLoader>().FromComponentsOn(gameObject).AsSingle();
        }

        void Mvp()
        {
            ModelInstaller.Install(Container);
            ViewInstaller.Install(Container);
        }

        ConcreteBinderGeneric<TContract> Bind<TContract>() => Container.Bind<TContract>();

        FromBinderNonGeneric BindInterfacesTo<TContract>() => Container.BindInterfacesTo<TContract>();
    }
}