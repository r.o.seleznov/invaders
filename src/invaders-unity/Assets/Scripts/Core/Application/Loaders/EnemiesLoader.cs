using System.Collections.Generic;
using Mechanics.ArmyLogic;
using Mechanics.EnemyLogic;
using UnityEngine;

namespace Core.Application.Loaders
{
    /*
     * This class will read an enemies data from the scene.
     */

    interface IEnemiesLoader
    {
        List<IEnemy>[] Load();
    }

    sealed class EnemiesLoader : MonoBehaviour, IEnemiesLoader
    {
        List<IEnemy>[] IEnemiesLoader.Load()
        {
            var holder = GetComponentInChildren<ArmyView>().transform;
            var enemies = new List<IEnemy>[holder.transform.childCount];
            int columnsCount = holder.childCount;

            for (var columnIdx = 0; columnIdx < columnsCount; columnIdx++)
            {
                var column = holder.GetChild(columnIdx);
                int rowLength = column.childCount;
                enemies[columnIdx] = new List<IEnemy>(rowLength);

                for (var rowIdx = 0; rowIdx < rowLength; rowIdx++)
                {
                    var enemy = column.GetChild(rowIdx).GetComponent<EnemyView>();
                    enemy.Id = InstanceId<IEnemy>.Next;
                    enemies[columnIdx].Add(new Enemy(enemy.Id, enemy.name, enemy.ScorePoints));
                }
            }

            return enemies;
        }
    }
}