using Core.Application.Factories;
using Mechanics.LevelLogic;
using UnityEngine;
using Zenject;

namespace Core.Application.Loaders
{
    /*
     * This class instantiates or re-instantiates level prefab.
     */

    interface ILevelLoader
    {
        void Load();
        void Unload();
    }

    sealed class LevelLoader : MonoBehaviour, ILevelLoader
    {
        [Inject] readonly IPrefabFactory _factory;
        [Inject] readonly LevelType _type;

        GameObject _levelOnScene;

        public void Load() => InstantiateLevel();

        public void Unload() => DestroyLevel();

        void InstantiateLevel() => _levelOnScene = _factory.Create(_type.LevelPrefab, transform);

        void DestroyLevel()
        {
            if (_levelOnScene)
                Destroy(_levelOnScene);
        }
    }
}