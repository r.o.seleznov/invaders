using Mechanics.ArmyLogic;
using Mechanics.LevelLogic;
using Mechanics.ThreatLogic;
using Mechanics.UfoLogic;
using UnityEngine;
using Zenject;

namespace Core.Application
{
    [CreateAssetMenu(fileName = "GameAppSettingsInstaller", menuName = "Installers/GameAppSettingsInstaller")]
    sealed class AppSettingsInstaller : ScriptableObjectInstaller<AppSettingsInstaller>
    {
        [SerializeField] UfoType Ufo;
        [SerializeField] LevelType Level;
        [SerializeField] ThreatType Threat;
        [SerializeField] ArmyAttackType ArmyAttack;

        public override void InstallBindings() => Container.BindInstances(Ufo, Level, Threat, ArmyAttack);
    }
}