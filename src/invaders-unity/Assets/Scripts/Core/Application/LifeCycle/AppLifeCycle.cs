using System;
using Core.Application.Loaders;
using Core.Mvp;
using UI;
using UnityEngine;
using Zenject;

namespace Core.Application.LifeCycle
{
    interface IAppLifeCycle : IStart { }

    sealed class AppLifeCycle : MonoBehaviour, IAppLifeCycle, IInitializable, IDisposable
    {
        [Inject] IHUD _hud;
        [Inject] ILevelLoader _levelLoader;
        [Inject] IModelLifeCycle _modelLifeCycle;

        void IStart.Start()
        {
            ReloadLevel();
            RestartModel();
        }

        void IDisposable.Dispose() => _modelLifeCycle.Init();

        void IInitializable.Initialize() => _hud.Open(Screens.StartScreen);

        void ReloadLevel()
        {
            _levelLoader.Unload();
            _levelLoader.Load();
        }

        void RestartModel()
        {
            _modelLifeCycle.Clean();
            _modelLifeCycle.Init();
            _modelLifeCycle.Start();
        }
    }
}