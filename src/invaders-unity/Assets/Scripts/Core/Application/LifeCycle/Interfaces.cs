namespace Core.Application.LifeCycle
{
    interface IInit
    {
        void Init();
    }

    interface IClean
    {
        void Clean();
    }

    interface IStart
    {
        void Start();
    }
}