using UnityEngine;
using Zenject;

namespace Core.Application.Factories
{
    sealed class FactoriesInstaller : Installer<FactoriesInstaller>
    {
        public override void InstallBindings() => 
            Container.BindFactoryCustomInterface<GameObject, Transform, GameObject, PrefabFactory, IPrefabFactory>();
    }
}