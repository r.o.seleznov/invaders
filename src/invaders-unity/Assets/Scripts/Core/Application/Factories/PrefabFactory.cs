using UnityEngine;
using Zenject;

namespace Core.Application.Factories
{
    interface IPrefabFactory : IFactory<GameObject, Transform, GameObject> { }

    sealed class PrefabFactory : PlaceholderFactory<GameObject, Transform, GameObject>, IPrefabFactory
    {
        [Inject] IInstantiator _instantiator;

        public override GameObject Create(GameObject prefab, Transform parent) =>
            _instantiator.InstantiatePrefab(prefab, parent);

        public override void Validate() => _instantiator.Instantiate<GameObject>();
    }
}